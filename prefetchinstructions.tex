%!TEX root = main.tex

Software prefetching is supported by x86, AMD64, aarch32 and aarch64 instruction set architectures. This chapter briefly summarizes the instructions. The following instructions can be injected into the code in run-time by special frameworks, in compile-time by compilers, or in coding-time by programmers. 

\section{X86-AMD64}
Intel started to support prefetch operations with Pentium 3 by adding prefetch instructions to instruction set architecture. Intel's instruction sets have more than one prefetch instruction each referring to individual locality or temporality option. Even though the exact prefetch instruction varies from architecture to architecture, all types of x86 can be summarized with the following sample set: PREFETCHT0, PREFETCHT1, PREFETCHT2, PREFETCHNTA. Here, the last part of PREFETCH instructions indicates the locality or the temporality option. 

Locality for a prefetch instruction is defined as the destination while the temporality is related to the eviction policy for the prefetched piece of data. The information for each option is given in Table \ref{table:intel_prefetch_instructions} 

\begin{table}[h!]
\centering
\caption{X86/AMD64 PREFETCH INSTRUCTIONS}
\begin{tabular}{l l c c c l} 

 \textbf{Instruction}    & \textbf{C Parameter}     & \textbf{L1}    & \textbf{L2}    & \textbf{L3}    \\    %& Description \\
 \hline
 PREFETCHNTA         & \_MM\_HINT\_NTA      & X       &         &       \\    %& Data is non temporal. Prefetch it to L1 cache and evict in the first conflict \\
 PREFETCHT0         & \_MM\_HINT\_T0        & X       & X       & X     \\    %& Prefetch the data to all levels of cache \\
 PREFETCHT1         & \_MM\_HINT\_T1        &         & X       & X     \\    %& Prefetch the data to 2nd and 3rd levels only \\
 PREFETCHT2         & \_MM\_HINT\_T2        &         &         & X     \\    %& Prefetch the data to 3rd level only \\
 \hline
\end{tabular}
\label{table:intel_prefetch_instructions}
\end{table}

Compilers and frameworks can inject particular PREFETCH instructions as a part of the optimization process. However for the programmers, it is not as straightforward. Because the locality and the temporality characteristics for each prefetch operation is indeed hard to predict. Still, PREFETCH instructions are exposed to the higher level programming languages such as C/C++. 

The \_mm\_prefetch and \_builtin\_prefetch functions of C/C++ are directly compiled to PREFETCH instructions. Prototype of \_mm\_prefetch is \textbf{void \_mm\_prefetch(char * pointer , int hint);} where pointer is the pointer of the data to be prefetched and hint gets one of the values in C Parameter column of Table \ref{table:intel_prefetch_instructions}

NTA (Nontemporal) option indicates that the particular data is not expected to be used again shortly. It can be a write-back request for example. Thus, the prefetched data with NTA option is not marked as ``Last Recently Used'' and is evicted at the first related conflict. Also, NTA option bypasses higher levels of cache and directly prefetches the data to the first level cache. In the technical documentation, the implementation details of this option are not included. However, it is guaranteed that this option does not harm any cache coherency protocol.

T0, T1, and T2 options refer to L1, L2, and L3 caches respectively as the closest point of the prefetch destination. The programmer should be careful about the timing of the prefetch request and the possible cache pollution to decide among these three options. However because of the abstraction layers between hardware and the application through the operating system, expecting the programmer to predict the timing of prefetch instruction is not realistic for the most cases.    

\section{ARM 32 Bit Architecture}

Starting from the ARMv7 architecture 32 bit ARM architectures support simple prefetch operation by PLD instruction. PLD (Pre-Load) instruction only gets the address of the data as a parameter. PLD does not have any locality nor temporality option. The \_builtin\_prefetch function produces PLD instruction for ARMv7 architecture.

Using \_builtin\_prefetch function can be tricky. GCC Reference limits the support of \_builtin\_prefetch function according to the target architecture. Therefore, target architecture is supposed to be specified in the compile options as \textbf{-march=armv7-a} 

\section{ARM 64 Bit Architecture}
Starting from ARMv8 architecture 64-bit architectures support a more complicated prefetch instruction which can be specialized according to the locality, the temporality, and the purpose of the prefetch operation. The PRFM instruction (Prefetch Memory), takes two arguments: The first one specifies the options about the purpose, the locality, and the temporality while the second argument gives the address of the data.  

\begin{table}[h!]
\centering
\caption{AARCH64 PREFETCH INSTRUCTION REFERENCE \cite{arm_reference_prfm} }
\begin{tabular}{l c c c } 

 \textbf{Instruction}    & \textbf{Type}     & \textbf{Target}    & \textbf{Policy} \\    %& Description \\
 \hline
 PRFM                     & PLD / PST            & L1 / L2 / L3        & KEEP / STRM         \\    
 \hline
\end{tabular}
\label{table:arm64_prefetch_instruction}
\end{table}

The first argument of the instruction consists of the combination of type, target, and policy. For example, the first parameter of \textbf{PRFM PLDL2STRM ADDR} is defined as the following: PLD means that the purpose of prefetch is load while PST refers to store. Target determines the locality of the prefetched data and policy tells whether it will be stored for a while or removed immediately. Here STRM option is similar to Intel's NTA option, but it can be applied in all levels of cache.

In this project, ARM64 codes are compiled with a cross compiler. One of the challenges in this project was the lack of cross compiler's support for PRFM instruction. PRFM instruction is supported by GCC 5.0 and newer versions while the cross compiler in Ubuntu's repositories based on GCC 4.X. In this project, as a side job, another cross-compiler was built based on GCC 5.4.0. Building a cross compiler is not a straightforward process. Fortunately, the builder script of Jeff Preshing worked by a few modification according to the needs of GCC 5.4.0. The script, used for this project, is accessible as a snippet on bitbucket \cite{script_bcc}.

\section{Evaluating the Prefetcher}

To prove that the prefetch instruction is supported by the particular hardware and the compiler, the following procedure was applied. 

As the first step, workload code was compiled with the \textbf{-S} flag to export the assembly code. Corresponding instructions for each ISA were found in the assembly code. 

The second step is to test if prefetch instructions help to the performance or not. In other words, if it is implemented and working properly on the given platform. This evaluation was processed only for x86 instructions, since the used real system is x86 based. 

A large linked list of elements is traversed. Each element allocates 64-bytes which is the size of a cache line. Linked list implementation was made by using an array. Array allocates consecutive addresses for each element consequently increasing the locality as well as the hit rate of the cache. On the other hand, to be able to observe the effect of the prefetch instruction the accuracy of hardware level prefetchers should be decreased with application-level modifications. The elements of the linked list are connected to each other with big jumps to lower the locality.

\begin{figure}
  \centering
  \includegraphics[width=0.8\linewidth]{images/PrefetchTestDataStructure@1x.eps}
  \caption{Data Structure of An Element of The Linked List}
  \label{fig:data_structure}
\end{figure}

Figure \ref{fig:data_structure} represents the data cell, the linked list, and the memory allocation pattern. As clearly seen, each element is linked to an element in the other half of the array. As the linking rule is based on modulo calculation, the linked list follows a circular linking. Mathematically, the only way of having the maximum distance for all links is to jump over half of the array for each link. 

Following the steps of traversal reveals that some of the elements are accessed multiple times. Accessing an element for the first time normally causes a cache miss, while the following accesses may hit in the cache. The improvement of prefetch is expected to be observed in accessing for the first time. So that every element has a \textbf{touched} variable. The average access time for each cell is calculated separately for touched and untouched situations. 

Pseudo code of the test workload is represented in Algorithm \ref{algorithm:prefetch_test_full_code}.

\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{images/PrefetchTestResults.png}
  \caption{Average Access Times}
  \label{fig:prefetch_test_results}
\end{figure}

Figure \ref{fig:prefetch_test_results} is the histogram of the access time measurements. The blue and the red colored bars represent the baseline (no prefetching) while yellow and green bars are affected by the prefetch instruction. As it is clearly seen, without the prefetch instructions, touched cells can be accessed in 290 ns. while untouched ones are accessed in 351ns. On the other hand, when prefetch instructions are enabled, touched cells can be obtained in 291ns. just like the baseline while prefetch instructions pull the access latency of untouched cells from 351 ns to 295 ns. The average speedup of first-time access is almost 19\% which is more than enough to prove that the prefetch instruction does work.