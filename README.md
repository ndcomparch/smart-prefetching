## Technical Report 

The latest version of the report is here in pdf format: [Preliminary Studies for Smart Prefetching on Distributed Systems](https://bitbucket.org/ndcomparch/smart-prefetching/raw/master/main.pdf)