%!TEX root = main.tex
Prefetching is a widely used technique for performance improvement which exploits the benefits of overlapping the memory access time with the computation time by requesting the relevant data in advance. Various methods of prefetching has been used in software and hardware levels. Even though the implementation details vary among different domains, the dynamics of the prefetching and the parameters of the prefetchers remain the same.

\section{Dynamics of Prefetching}
The success of a particular prefetch operation depends on the accuracy and the timing whereas the overall impact of a prefetching technique depends on the number of the prefetch operations as well. 

The accuracy of a prefetcher defined as the ratio of benefited prefetched data to all prefetched data. The larger number of prefetched blocks are accessed before the eviction, the higher accuracy is. In contrast, the more prefetched blocks are evicted without any access, the accuracy decreases. 

\[
    \label{equation:prefetch_accuracy}
    {Accuracy} = 1 - \frac{Number of Prefetched But Never Accessed Blocks}{Number of Prefetched Blocks}
\]

Apart from the accuracy of a prefetcher, every single successfully prefetched block is expected to improve the performance by diminishing the memory cost. However, the accuracy is strongly related to the overall performance of a thread as well as the whole system through the cache pollution.

\subsection{Accuracy, Cache Pollution, and Performance}
Due to the performance concerns, caches get smaller closer to the processor. Less space causes an eviction of a cache block whenever a new data block is fetched. In the case of the old data is needed shortly, an extra memory request takes place because of the newly fetched data. If the thread needs both data blocks, this is an inevitable cost. If different threads fetch the old and the new data blocks, one thread causes the other thread to slow down. The term `cache pollution' mainly used for such kind of multi-thread scenarios as shown in Figure \ref{fig:intro_cache_pollution}. 

\begin{figure}[style={figure}]
  \centering
  \includegraphics[width={0.8\linewidth}]{images/Intro_CachePollution.pdf}
  \caption{Cache Pollution}
  \label{fig:intro_cache_pollution}
\end{figure}

However, the cache pollution happens even for a single thread if any prefetcher is active. Cache pollution happens in such scenarios in case an unnecessarily prefetched block evicts a necessary block. Here the words `unnecessarily prefetched' is related to the accuracy of a prefetcher. 

As a chain reaction, if the accuracy of a prefetcher decreases, it prefetches many unnecessary data blocks and evicts many data blocks from the cache. Evicting many blocks increase the possibility of the cache pollution which triggers extra memory accesses and decreases the performance of the thread. 

From another point of view, prefetch requests increase the data traffic alone. Moreover, further evictions and re-fetches, which are caused by wrong prefetches, add more traffic to the memory, to the disk, and even to the network. Finally, low accuracy of the prefetcher may cause a breakdown in the overall performance of a system. 

\subsection{Timing}
When a prefetch instruction comes into the pipeline or a hardware level prefetcher mechanism starts a prefetch operation, first level cache receives a prefetch request. After a quick search in the cache and the queues, the request may advance up to the last level cache as long as the address does not hit. If it does not hit in any level of cache, memory receives a read request, which may create severals i/o interrupts by using syscalls to access to the disk or even to the other nodes in the network. Once the address is found, the data is fetched to the desired level of cache. Clearly, the time cost of the whole operation varies depending on the locality of the data, configuration of the hardware, and the memory allocation policy. 

\begin{figure}
  \includegraphics[width={\linewidth}]{images/Intro_PrefetchTimeline.eps}
  \caption{Prefetch Timeline \cite{when_prefetch_works} }
  \label{fig:intro_prefetch_timeline}
\end{figure}

Each prefetch operation has three milestones as the Figure \ref{fig:intro_prefetch_timeline} shows. INIT: when the prefetch request is created, IN: when the prefetched data comes into the cache, and OUT: when the data is evicted out. 

Any prefetch operation experiences all the milestones in the given order in Figure \ref{fig:intro_prefetch_timeline}. The success of the prefetch, depends on the time that the real load/store operation is needed. If the load/store instruction comes between INIT and IN, the prefetch operation is considered as late, since the prefetched data is not ready yet. If the load/store appears after OUT, it means that the gap between the prefetch request and the access is too long. So that, the prefetched data can not survive until it is needed. The prefetching helps if the program requires the data between IN and OUT. Therefore, a prefetch request is not supposed to be initiated too early or too late but in a certain window. 

\subsection{Prefetcher Parameters}
\subsubsection{Threshold}
No matter which strategy is used, the accuracy of the prefetcher is always a matter of prediction. For any method, each prefetch address has some relation with the previously fetched data. The relation could be very different from case to case like: spatial relations, patterns, semantic relations, etc. Yet, a metric of distance can be defined to represent the weight of the relation. 

For example, the weight of a pair of vertices in a social network can be the similarities between two people. The possibility of accessing any vertex after a particular vertex is related to the weight of the relation in between. For each link, the weight helps the system to decide whether the data is worth to prefetch or not. An optimized threshold value may improve the performance of the prefetcher mechanism. Therefore, threshold becomes a vital parameter for the prefetchers.

\subsubsection{Aggressiveness}
A particular piece of data usually has links with more than one piece of data. For example, a vertex in a graph is often related with many other vertices in the same graph. Even though the weight of the links can vary, many of them can be above the threshold value.

In such cases, comes the question: `How many prefetch requests should the prefetcher create?' Aggressiveness is the parameter which defines this characteristic, in other words, the first order greediness of the prefetcher.

\subsubsection{Depth}
Similar to the fact that the aggressiveness controls the greediness in the breadth-first direction, the depth of the prefetcher manages the eagerness in the depth-first direction. 

The larger depth parameter creates deeper recursive prefetch requests while the prefetch requests of the aggressiveness run in parallel. 

Depending on the traversal organization of the algorithm, the balance of the depth and the aggressiveness may slide from one side to the other among different threads. Similarly, the early and the late stages of the same thread may need distinct balance points for the aggressiveness and the depth.

\subsubsection{Locality}
As described in Chapter \ref{chap:prefetchinstructions}, prefetch instructions allow a user to set prefetch destination as any level of cache. This feature provides more control on the cache pollution and the timing. For example, if a prefetched data block is brought to the second level cache, instead of first level, it still reduces the memory cost and the possibility of the cache pollution in the L1 cache.

\subsubsection{Temporality}
Prefetched data can be marked as ``least recently used'', according to the option set in prefetch instruction. In set-associative cache structure if a particular prefetched data is nontemporal, evicting it before any other blocks in the same set helps to avoid cache pollution.  

\section{Prefetching on Distributed Systems}
Prefetching dynamics of single core machines are different than of many-core machines as the parallelism redefines the rules in the scheduling and the resource sharing. Both single and many-core systems are still a single machine. With the increase in the demand of many-core architectures after the `power wall', many-core specific problems have been well studied for prefetching techniques. However, the era has changed again with the emerging big data problems and the increasing demand for distributed architectures and cloud computing. Although the network of distributed architecture is considered as similar to the network on chip systems, the differences in the implementation of the networks change the rules for prefetching.

First of all, the nodes of the network are separate computers and the link between them can be provided by a local network as well as the world wide web. So that the latency between nodes may differ according to the bandwidth and the number of hops in runtime whereas communication between nodes in a NoC remains the same.

The second important distinction is the storage. Nodes of a NoC usually have a shared last-level-cache, memory, and disk. On a distributed system, the storage is also distributed for most cases. Even though distributed storage can be used as a virtual unified storage device, the timing concerns can not be hidden for prefetch related works. Different than the centralized storage, accessing to a piece of data may cost different than another piece of data.

Another contrast of two systems is the frameworks. On NoC systems, applications directly use the load/store instructions to reach anywhere on the computer. If the data is not in the memory, a syscall is created to reach to the disk. On the other hand, distributed systems require applications to use frameworks such as MPI \cite{mpi_book}.

To sum up, despite the fact that the distributed systems provide a strong abstraction for users to hide the complexity, from the prefetching point of view, the complexity behind the scenes makes the problems distinct from the already-solved problems of NoC devices. 

\section{Project Definition}
Prefetching has been studied for more than a decade and has been improved successfully. However, the mutation of the architectures and the applications alters the problems, even brings new problems. Through the recent years, the needs of the whole society are significantly shifted to handling large size graphs on distributed architectures, in other words, shifted to the big data. Even though the nature of the problems and the platforms has changed, the same prefetching techniques remain. This project investigates the ways of improving the prefetching methods according to the today's needs. 

Instead of using the pattern recognition based hardware level approaches, the proposed technique comes with higher accuracy by exploiting the advanced link prediction methods which are used for graph analysis. Therefore, the proposed method can only be implemented by using the prefetch instructions. 

Another bullet point about the proposed technique is the need for adaptivity. Since the characteristics of the graphs in big data applications keep changing in runtime, the prefetcher may need to adapt. In a similar way, the applications frequently migrate from node to node on distributed systems because of the utilization and the maintenance requirements. Therefore another adaptation is needed for the dynamic hardware. Also, the access patterns of the algorithm may change through the early and the late stages of the algorithm which may require some parameters to change like aggressiveness or depth. Therefore another adaptation is needed for the algorithm. 

To sum up, the proposed technique is a software-level approach, which supports adaptivity in 3 degrees of freedom: the hardware, the algorithm, and the data set. Here, the hardware can be considered as 3 separate pieces such as: the network, the nodes, and the data storage devices. 

As shown on Figure \ref{fig:intro_proposed_prefetcher}, the prefetcher parameters are defined by a decision tree, based on the features of the hardware, the dataset, and the algorithm. Although the decision tree is trained offline, the feedback of performance counters is also populated into the decision tree for minor optimizations. The feedback includes the data about the prefetcher accuracy, the cache utilization, the traffic on the network, the latency of disk operations, etc. 

\begin{figure}
  \centering
  \includegraphics[width={0.8\linewidth}]{images/Intro_ProposedSystem.eps}
  \caption{Smart Prefetcher}
  \label{fig:intro_proposed_prefetcher}
\end{figure}

\subsection{Novelty of the Project}
Prefetching techniques are well studied for the single node architectures in both hardware and software levels. Also, a significant amount of work on big data subjects exists in the literature about the prefetching (mostly using the terms caching or pre-caching). The advanced techniques in both fields exploit the benefits of machine learning and neural networks as well. However, to the best of our knowledge, this project is the first software level prefetching attempt to create a smart interlayer which is populated by both the dynamic and the static features of the hardware, the dataset, and the algorithm.