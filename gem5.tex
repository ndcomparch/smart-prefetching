%!TEX root = main.tex
Gem5 is a widely used cycle-accurate architecture simulator. Gem5 has several advantages over McSimA+ such as multi-ISA support, full-system simulation, deterministic simulation results, active developer community, and more populated user community. On top of all, gem5 started to support distributed system simulation with a brand new update. Distributed system simulation is still not a part of the stable version when this report is submitted but is already merged into developers repository (gem5-dev). The patch pd-gem5 was first announced in the MICRO conference with a tutorial in December 2015 and was published in the IEEE Computer Architecture Letters in early 2016 release. \cite{pd_gem5_micro,pd_gem5_archletters}

gem5 provides two simulation modes which are the syscall emulation and the full system simulation. Syscall Emulation runs the benchmark on the specified architecture and emulates the syscalls of the workload. On the other hand, the full system simulation uses a real disk image and a Linux kernel to run all the syscalls. Therefore, syscall emulation runs faster but not as accurate as the full system simulation. Since the prefetching is directly related with the disk and the network, gem5 should be used in full system simulation mode for this project. Due to the similar concerns, pd-gem5 supports only the full system simulation.  

The configuration of the full system is defined by \textit{fs.py} script. To configure the system, a user needs to pass the relevant parameters to the fs.py python file. The following command is an example, initiating full system simulation which is extracted from the scripts of pd-gem5. The command simulates a quad-core 64-bit ARM machine with Ubuntu operating system. 

\begin{lstlisting}[style=script, caption=Example Full System Simulation Command]
    build/ARM/gem5.opt -d ARM64_Output                               \
    configs/example/fs.py                                            \
    --cpu-type=atomic                                                \
    --caches --l2cache                                               \
    --l1d_size=4kB                                                   \
    --l1i_size=4kB                                                   \
    --l2_size=2MB                                                    \
    --num-cpus=4                                                     \
    --machine-type=VExpress_EMM64                                    \
    --disk-image=${M5_PATH}/disks/aarch64-ubuntu-trusty-headless.img \
    --kernel=${M5_PATH}/binaries/vmlinux.aarch64.20140821            \
    --dtb-filename=${M5_PATH}/binaries/vexpress.aarch64.20140821.dtb 
\end{lstlisting}

\subsection{Installation}
The gem5 simulator uses many external packages. Thus, dependency requirements should be satisfied before the compilation of the simulator. The full list of dependencies can be found in the official wiki of gem5. Most of the packages already exist in the apt-get repositories of Ubuntu. Therefore, on an Ubuntu host machine, satisfying the dependencies is straightforward. 

The full system simulation may take several hours or even days, depending on the configurations and the workloads. Fortunately, CRC can be used to run it faster. However installing the gem5 dependencies on CRC is not a straightforward process because of the security-related restrictions. Since the user does not have any administrative permissions, all the dependencies should be installed into the home directory which brings the necessity of building and installing from the source code. As an output of a week-long collaboration with Dr. Dodi Heryadi from HPC group of University of Notre Dame, the guide of installing gem5 to CRC is created. \cite{gem5_guide_for_crc}

\subsection{Running Tips}

\begin{figure}
  \label{fig:full_system_components}
  \centering
  \includegraphics[ width={\linewidth} , height={0.4\linewidth} , keepaspectratio ]{images/ExpSetup_gem5FullSystemComponents.eps}
  \caption{The Components of the Full System Simulation}
\end{figure}

The full system simulation has several components as depicted in Figure \ref{fig:full_system_components}. The gem5 binary mimics the hardware of a computer while the Linux kernel runs on the virtual hardware as the operating system. The file system substitutes a part of operating system and the stored data. Finally, the workload is saved as an application in the file system. Therefore each time the workload changes, the copy in the disk image needs synchronization. Here synchronization means to mount the image to the host device and to copy the files. An example script for synchronization is represented in Script \ref{script:workload_sync}

\begin{lstlisting}[style={script}, caption={Referring to the Disk Image and the Kernel}, label={script:workload_sync}] 
    DISK_IMAGE = path/to/disk/image
    WORKLOAD = path/to/worklaod
    sudo util/gem5img.py mount ${DISK_IMAGE} /mnt
    sudo cp -r ${WORKLOAD} /mnt/home/gem5/
    sudo util/gem5img.py umount /mnt
\end{lstlisting}

gem5 provides the m5 binary under util/m5 directory as an API for the workloads to allow them to get statistics. Once the gem5 compiled for any architecture, a new m5 binary should be copied into the /sbin directory of the disk image. Script \ref{script:m5_sync} illustrates how to compile and copy the m5 binary to the disk image. The simulator gives individual makefiles for each architecture. One should give the proper make file to the `make all' command as each makefile calls a separate cross-compiler according to the target architecture.   

m5 binary can be executed on terminal just by typing m5 help to the telnet or m5term session. However, workloads frequently require executing m5 binary during the execution of the workload itself. Furthermore, the experiments are usually intolerant to probe cost. Fortunately, gem5 comes with the necessary libraries to allow m5 functions to be called by C/C++ and JAVA codes. An example of calling m5 functions within the C++ code can be found in the Algorithm \ref{algorithm:prefetch_test_full_code_m5}. The important point is to compile util/m5/m5op\_<architecture>.S file with the source code as shown in the make file in Script \ref{script:makefile_prefetchtester_m5}. 

\begin{lstlisting}[style={script}, caption={Preparing the m5 binary for the disk image}, label={script:m5_sync}] 
    DISK_IMAGE = path/to/disk/image
    cd ${M5_PATH}/util/m5
    make clean
    make all -f Makefile.<architecure>
    sudo util/gem5img.py mount ${DISK_IMAGE} /mnt
    sudo cp ${M5_PATH}/util/m5/m5 /mnt/sbin/
    sudo util/gem5img.py umount /mnt
\end{lstlisting}

Since a real operating system runs on a cycle-accurate simulator, the simulation can be expensive in terms of time and memory. The simulation starts with the boot process of the operating system which usually takes at least 30 minutes on an average host machine and can easily reach to hours depending on the CPU mode and the hardware configuration. Checkpointing helps to reduce the cost of the recurring simulations by skipping the boot process. 

Referring to Figure \ref{fig:full_system_components}, as long as the gem5 binary remains the same, the hardware of the virtual system does not change. Thus, even if the workload or some user files change, the same checkpoint still works to skip the boot process. On the other hand, slightly changing only one configuration of the hardware may cause inconsistency in the whole system. Therefore, even if just the size of one cache is changed, the operating system should boot again. 