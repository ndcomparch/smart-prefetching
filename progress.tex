%!TEX root = main.tex
The project was first initiated by Dr. Sharon Hu and Dr. Michael Niemier and was assigned as a course project to Keith Feldman \footnote{kfeldman@nd.edu} under the supervision of the course instructor Dr. Oguz Ergin. Because of the technical difficulties, the project had to stop until the simulation environment is changed. Later, Abdulah Giray Yaglikci \footnote{agirayyaglikci@gmail.com} took the responsibility of the project as a graduate student. Since then, the project has advanced through 3 phases. The phases are summarized in this chapter in chronological order to provide a comprehensive snapshot of the project. 

\section{Link Prediction}
The first problem was to achieve high accuracy by using advanced methods of link prediction. At this stage, the workload of the project is defined as Clustering Coefficient and Common Neighbors was preferred for the link prediction. 

\subsection{Common Neighbors}

The number of common neighbors of a pair of vertices reveals the relational distance between the vertices. The vertices are closer if the number of common neighbors is higher. For instance, a given pair of individuals in a social network can represent a friendship in real life even though individuals are not friends of each other on the given database. The friendship probability of any pair increases with the number of shared friends. Therefore, the number of common neighbors of two vertices depicts the probability of a future edge between them.

\begin{figure}
  \centering
  \includegraphics[width=0.8\linewidth]{images/Progress_CommonNeighbors.eps}
  \caption{Common Neighbors of a Pair of Vertices}
  \label{fig:progress_commonneighbors}
\end{figure}

The number of common neighbors is naively counted by calculating the intersection of edge lists of two vertices as shown in Figure \ref{fig:progress_commonneighbors} and Algorithm \ref{algorithm:common_neighbors}. For each vertex, the result of `Common Neighbors' is used as a key value to store the link predictions in order. Thus, at the end of `Common Neighbors' routine, each vertex has a sorted list of link predictions. 

`Common Neighbors' is used as a pre-process to prepare the graph for the actual workload which is `Clustering Coefficient.'

\subsection{Clustering Coefficient}
\begin{figure}
  \centering
  \includegraphics[width=0.8\linewidth]{images/Progress_ClusteringCoefficient.eps}
  \caption{Clustering Coefficient of a Vertex}
  \label{fig:progress_clusteringcoefficient}
\end{figure}

`Clustering Coefficient' is chosen as the workload for this project across the meetings with iCeNSA. `Clustering Coefficient' is defined as a metric of connectivity of a particular vertex of the graph with the neighbors of it. The clustering coefficient of a vertex can be in the range of [0,1]. If the vertex is strongly connected to all neighbors, the clustering coefficient should be 1. 



The most straightforward calculation of clustering coefficient for vertex v is shown in Figure \ref{fig:progress_clusteringcoefficient}, Algorithm \ref{algorithm:clustering_coefficient} and represented by the following equation. 

\[
    \label{equation:clustering_coefficient}
    {Clustering Coefficient} = \frac{Number of Triangles}{Number of Possible Triangles}
\]

\section{Hidden Hint Method on gem5}
The prefetching mechanism works on a hard-coded template of a data structure. It scans all the fetched data. In cases of any match for both the header and the footer, the word in between is considered as the address to prefetch. Another type of hint structure is defined to hold multiple prefetch addresses as shown in Figure \ref{fig:progress_hintstructure}.

\begin{lstlisting}[style=script, caption={C++ Definition for the Hint Structure}]
struct __attribute__ ((packed)) node {
	uint32_t header = 0x0F0F0F0F;
	node * lds_pointer;
	uint32_t footer = 0xF0F0F0F0;
} ;
\end{lstlisting}

The idea is to use the prefetching without changing the source code. Just by modifying the data structures of the graph libraries (for example, the boost graph library), the prefetcher can be distributed to a broad spectrum of applications.

However, the proposed method at this stage needed several parts of implementation: 

To prepare the hint structure in the source code, the pre-processing algorithm should be run on the same dataset for link prediction. Therefore the source code of the application does not need to be changed, but a pre-process should be added at the beginning of the applications.

The hardware part of the implementation needs to scan the recently fetched data in the cache to detect hints. Every time a hint structure is exposed, a prefetch request should be created. So that, the related data can be prefetched without any modification in the source code. 

\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{images/Progress_HintStructure.eps}
  \caption{The Hint Structure}
  \label{fig:progress_hintstructure}
\end{figure}

The pre-process routine calculates the hint pointers. Therefore, all the addresses are calculated in the virtual domain. However, most of the systems translate addresses from virtual to physical inside the core. Other than few exceptional products, all levels of caches use physical addressing.  Consequently, hardware-level implementation needs to access the TLB and translate the detected hint pointer. Thus, the hardware-level implementation becomes complicated. 

The first attempt of implementing the hardware part of the prefetcher on gem5 simulator was tried by Keith Feldman. However, the realistic restrictions of gem5 did not allow to access the TLB from the cache level. As a solution, the simulator of the project had been changed from gem5 to McSimA+ in consultation of Dr. Sheng Li, an alumni of the University of Notre Dame and the author of McSimA+, CACTI, and McPAT.  

\section{Hidden Hint Method on McSimA+}
Switching to McSimA+ had several advantages in the early stages of the project such as full support from one of the authors, network on chip support, and allowing to use the virtual addresses across the full simulator. Also, the decoupled structure of McSimA+ reduced the wall clock time of the simulation significantly.

Abdullah Giray Yaglikci took the project over at the beginning of this phase. As a result of this nonstraightforward phase, the LDS (Link Data Structure) prefetcher is implemented into McSimA+ as shown in Figure \ref{fig:progress_mcsima+implementation}. However, the speedup was negligible even though the address translation was by-passed.

\begin{figure}
  \centering
  \includegraphics[width=0.7\linewidth]{images/Progress_McSimA+Implementation.png}
  \caption{Implementation of Hint Structure on McSimA+}
  \label{fig:progress_mcsima+implementation}
\end{figure}

As a result of the process, another literature search has been made to investigate the reasons behind the low performance. Meanwhile, the prefetch support of the ISA's was discovered. 

\section{Prefetch Instructions on Real System}
Despite the designs are totally different, the proposed hint structure and the prefetch instructions have the same functionality. The prefetch instructions even have more capabilities like locality and temporality options. Therefore, the hint-structure idea is replaced by the idea of using the prefetch instructions. Hence, another phase of the project started.

\begin{figure}
  \centering
  \includegraphics[width=0.7\linewidth]{images/Progress_RealSystem.png}
  \caption{The Observed SpeedUp on a Real System}
  \label{fig:progress_realsystem}
\end{figure}

Since the prefetch instructions are not implemented in the back-end of McSimA+, a real system is preferred rather than the simulator. By using the real system, described in Chapter \ref{chap:experimental_setups}, 6\% speedup is observed on the `Clustering Coefficient' algorithm with the perfect link prediction as shown in Figure \ref{fig:progress_realsystem}. The related repository is kept private on BitBucket servers but can be accessed on demand. Please contact with Abdullah Giray  Yaglikci for permission request. 

\section{Prefetch Instructions on gem5}
The results of the real machine experiments validated the idea. To make progress on the larger plan and to reach the ultimate goal of the project, various hardware configurations were needed to be observed for a large number of algorithms and datasets. So that, the feature extraction and the training jobs would be statistically significant. Therefore, switching back to the simulator environment became necessary. 

As previously used simulators, McSimA+ and gem5 were evaluated again as the candidates for the new simulation environment. Because of the larger community, more deterministic simulation results, emerging distributed system support, and the support for multiple instruction set architectures, gem5 was chosen over McSimA+.

The full system simulation environment for gem5 experiments is prepared as described in Chapter \ref{chap:experimental_setups}. The same workload (Clustering Coefficient) was executed in the same way with the real system. Even though the observed speedup was 6\% on a real system, gem5 results were either negligible speedup or slowdown.

\begin{table}[h!]
\centering
\caption{THE PERFORMANCE RESULTS}
\begin{tabular}{l l r} 
 The Platform 				& Used Instruction 	& Gained SpeedUp 	\\
 \hline 
 AMD A10-5800K APU			& PREFETCHT0		& 6\%				\\
 NVIDIA Tegra X1 ARM 32-bit	& PLD				& 0.5\%				\\
 gem5 ARMv7 (aarch32)		& PLD				& 0.5\%				\\
 gem5 ARMv8 (aarch64)		& PRFM PLDL1KEEP	& -1\%				\\
 gem5 x86-AMD64 (64-bit)	& PREFETCHT0		& -1\%				\\
 \hline
\end{tabular}
\label{table:progress_performanceresults}
\end{table}

Table \ref{table:progress_performanceresults} represents the approximate values obtained on real systems and gem5. Among all, only the AMD A10 machine showed considerable speedup. 

ARM 32-bit architecture does not have any complicated prefetch operation but PLD (Pre-LoaD) as a substitute which is not successful on a real system as well. 

The results of ARM 64-bit and X86-AMD64 architecture models of gem5 are surprising. Prefetch instructions led slowdown, instead of speedup. The reason of no-speedup is discussed on the gem5-dev mailing list and concluded with the piece of information that currently, gem5 does not have proper implementations of the prefetch instructions in the core and the cache models.
