%!TEX root = main.tex
% Since the hardware level prefetchers are able to detect complex patterns of data access, software level prefetching is not equally successful for all architectures and applications. Also the software developer community rarely uses the prefetch commands since the common sense of programmers is highly optimistic about the hardware prefetchers, furthermore highly pessimistic about the software level prefetching.\par

AMD64 based system is used to observe the effect of software level prefetching in this project. Technical specs of the platform is given in Table \ref{table:real_system_specs} .\par
 
\begin{table}[ht!]
\centering
\caption{SPECS OF THE REAL SYSTEM}
\begin{tabular}{l r} 
 \hline
 Processor & AMD A10-5800K APU 3.8 GHz\\ 
 Operating System & Ubuntu 14.04 LTS (64 bit)\\
 L1 Data Cache & 16 kB 2 ways set assoc. \\
 L1 Instruction Cache & 64kB  \\
 L2 Cache & 2MB \\ 
 \hline
\end{tabular}
\label{table:real_system_specs}
\end{table}

The given system runs applications on an operating system (in this case Ubuntu). Therefore the routines of the system and the resource management policies bring non determinism to the time measurements. Generating reliable results is only possible by increasing determinism. The non determinism here has 2 major reasons: CPU scheduling and cache pollution. 

\subsection{Effect of CPU Scheduling}
The test case application is not the only application running on the system. Even though the computer has only one user, dozens of processes run at a given time to maintain the system and the required services. So that, the given application does not run on an assigned processor from the beginning, to the end continuously. Instead it experiences many interrupts and context switches. Thus, despite many benchmarking techniques, using wall clock for time measurement is misleading. Fortunately C/C++ provides necessary functions to measure the CPU time, which is the amount of time that a given process was active on CPU. Two widely used functions are \textbf{getrusage\(\)} and \textbf{clock\_gettime\(\)}. getrusage is a comprehensive analysis function which returns 16 different statistics such as page faults, context switches, memory occupation as well as the CPU time. On the other hand clock\_gettime returns only the CPU time. \par 

\begin{figure}
  \includegraphics[width=\linewidth]{images/getrusage_histogram.eps}
  \caption{The histogram of CPU time values of getrusage}
  \label{fig:getrusage_histogram}
\end{figure}

getrusage() was the first choice in this project to measure the CPU time. Preliminary measurements for a dummy code returned the histogram in Figure \ref{fig:getrusage_histogram}. The precision of Histogram \ref{fig:getrusage_histogram} (a) is 4 ms which is defined by the USER\_HZ parameter of the Linux Kernel. USER\_HZ parameter is defined as the number of jiffies in a second. More scientifically, it can be defined as the sample rate of resource statistics. Thus, increasing USER\_HZ parameter of Linux Kernel increases the precision of getrusage. However the max allowed value for USER\_HZ is 1000\footnote{The value is obtained from Linux Kernel 3.19.8}. Changing one parameter and compiling a new kernel increases the precision from 250 Hz to 1000 Hz as it is shown in Figure \ref{fig:getrusage_histogram} (b). However, 1 ms is still a too wide window to observe the effect of prefetching. On the other hand, clock\_gettime() function measures the CPU time in 1 ns accuracy for the same system. Therefore, clock\_gettime() function is preferred in terms of precision. \par

From another aspect, the variation of the way of measurement of clock\_gettime() and getrusage() is also important. For the timeline given in Figure \ref{fig:getrusage_vs_clock_gettime_timeline}, assume that a process is running with 2 threads T0 and T1. T0 and T1 are assigned to the cores in red and green cycles respectively. Blue cycles are used by all other threads on the system. So that T0's active windows are A,J,M,F while T1's are H,C,K,E,O in order.\par

The CPU time measurement by getrusage and get\_clocktime varies here. For each function and each configuration, the counted cycles for each thread are demonstrated in Table \ref{table:getrusage_vs_clock_gettime_timeline}. \par

\begin{figure}
  \includegraphics[width=\linewidth]{images/getrusage_vs_clock_gettime_timeline.eps}
  \caption{CPU Scheduling of 3 Threads on 2 Cores}
  \label{fig:getrusage_vs_clock_gettime_timeline}
\end{figure}

\begin{table}[ht]
\centering
\caption{\uppercase{Distinction of getrusage and clock\_gettime for CPU Time measurement (Refer to Figure} \ref{fig:getrusage_vs_clock_gettime_timeline})}
\begin{tabular}{l l l c c} 
 \hline
 \textbf{Function} & \textbf{Configuration} & \textbf{Counted Cycles} & \textbf{T0} & \textbf{T1} \\ 
 \hline
 \textbf{getrusage} 		& \multirow{2}{*}{rusage\_thread} 	& T0:A-H-J-C-K-M-E-O-F 				& \multirow{2}{*}{19} 	& \multirow{2}{*}{18} 	\\
 Precision: 1ms				& 									& T1:H-(2/3)A-J-K-C-M-E-O-(1/2)F 	&						& 						\\
 \hline
 \textbf{clock\_gettime} 	& \multirow{2}{*}{Default}			& T0: A-B-C-D-E-F					& \multirow{2}{*}{15} 	& \multirow{2}{*}{13} 	\\
 Precision: 1ns				& 									& T1: H-I-J-K-L-M-N-O				& 						& 					 	\\ 
 \cline{2-5}
 						 	& \multirow{2}{*}{Thread Time}		& T0: A-J-M-F						& \multirow{2}{*}{10} 	& \multirow{2}{*}{9} 	\\
 							& 									& T1: H-C-K-E-O						& 						& 					 	\\ 
 \cline{2-5}
 						 	& \multirow{2}{*}{Process Time}		& T0: A-1/3(H)-J-K-M-(1/3)E-F		& \multirow{2}{*}{9} 	& \multirow{2}{*}{10} 	\\
 							& 									& T1: H-(1/2)J-C-K-(1/3)M-E-O		& 						& 					 	\\ 
 \hline
\end{tabular}
\label{table:getrusage_vs_clock_gettime_timeline}
\end{table}

getrusage returns the total resource consumption of the process P for the individual threads, T0 and T1. However, the measurement for each thread returns the consumption of P during the particular thread was active. Thus, if different threads of the same process runs simultaneously, the overlap is included for each thread. getrusage's measurement policy can be observed by exploring Figure \ref{fig:getrusage_vs_clock_gettime_timeline} and Table \ref{table:getrusage_vs_clock_gettime_timeline} together. For example T1 starts running on Core 2 at the second cycle (H) and 14th cycle is the last cycle of T1 (O). Between these two timestamps T1 uses the cores for the cycles: H,C,K,E and O. On the other hand, T0 uses 2/3 of A, full of J and M, and 1/2 of F. So, the total consumption of the process P while T1 was active (the return value of getrusage) is the sum of T0's and T1's consumptions. On the other hand clock\_gettime returns the exact sequence of cycles for each thread when the thread configuration is selected. \par

In conclusion, clock\_gettime provides the accurate CPU time of each thread. Therefore, the number of other processes running on the same host machine during the experiment should not be a problem. Unfortunately, the observed variation on hundreds of measurements emphasizes the non determinism of the experimental setup. There comes the underestimated effect of the cache pollution on real systems. \par

\subsection{Cache Pollution}
Cache performance is frequently evaluated by running single benchmarks as McSimA+ simulator and gem5's system emulation tool do. Running one application at a time as benchmark is as realistic as running multiple applications simultaneously, if the focus of the research is on cache coherency, hardware reliability, security, or power aware design problems. However, performance of prefetching should not be evaluated on a single process scenario, as prefetcher performance is strongly related with memory allocation and eviction mechanism. Figure \ref{fig:expsetup_cachepollution} demonstrates the cache pollution caused by overlapping allocations at L1 cache. 

\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{images/ExpSetup_CachePollution.eps}
  \caption{Cache Pollution on A Real System}
  \label{fig:expsetup_cachepollution}
\end{figure}

For the given system and as an industry standard, each core has a first level private cache. Second level caches are private for some processors and shared for others while third level cache either is shared or does not exist in most processors. Cache pollution of shared caches is a well studied phenomena. Separate cores run several threads simultaneously. If some or all of the threads have a greedy pattern in memory access, they cause some unnecessary or non temporal data to be cached. Oftentimes, the simultaneous threads evict each other's data to fetch their own data to the same place in the cache. Increasing the greedy pattern of fetching, increases the eviction frequency. As a result, the overall performance of the system decreases. This problem is referred as cache pollution in the literature. \par

From another point of view, cache pollution does not have to be a result of simultaneous threads. It also occurs if several threads run on a single processor in intervals. After each context switch, the new thread evicts the data fetched by the previous thread. Therefore, the number of threads sharing the same processor affects the cache performance of individual threads. CPU scheduling policies, instant operating system routines, house keeping jobs, and interrupts bring nondeterminism to the experiment results by causing cache pollution. Due to the fact that prefetching contributes to overall performance through cache performance, cache pollution may reduce or eliminate the positive effect of prefetching. Hence, the number of other processes on real system should be decreased as much as possible. \par

Practically, closing the unnecessary windows, killing unnecessary processes and even shutting the X-window off helps to reduce the nondeterminism. However, killing all processes but the vital ones is not a straightforward job unless the user is an expert. Fortunately Linux provides killall5 command to kill all the processes but the vital ones. All the measurements of real machine experiments are done by using killall5 command. killall5 kills all non-vital processes even the terminal itself, but it keeps ssh server working. The CPU occupation of the remaining processes is observed as 0.3\%, after killall5 is executed .\par

\subsection{Obtaining Cache Stats}
On a real system, obtaining cache hit/miss rates is not always possible. It highly depends on the hardware and the operating system. Performance API (PAPI) is a tool which allows user to read performance counters \cite{papi_reference}. PAPI reads the hardware performance counters into the specified variables. Even though the whole process can be considered as a simple register move operation, the overhead of the tool is not negligible. Also, as expected, performance counters reflect a similar nondeterministic behavior as CPU time measurements because of the same problems of the resource sharing and the cache pollution. 

The measurements of PAPI have 2 components: the tare cost of PAPI tool and the contribution of the application. Statistical significance can be satisfied only if the tare cost of the PAPI tool is negligible compared to the cost of the application. Ideally, the workload given in Chapter \ref{chap:progress} should be analyzed for individual load operations, however single load operations are less costly then the PAPI routines. Therefore, the individual load operations couldn't be measured by PAPI but all load operations of a vertex or of a thread was measured separately. 

\subsection{Statistical Significance of The Real System Experiments}
The Clustering Coefficient workload described in Chapter \ref{chap:progress} was run on the real system with different prefetch-limit and dummy-delay configurations. Each value was obtained as the average of 500 experiments. Also the standard deviation of each average value is evaluated and only the measurements in 2-STD-wide window are included. 


